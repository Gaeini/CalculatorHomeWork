package ir.syborg.app.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;


public class ActivityCalculator extends Activity {

    String afterResult  = null;
    String beforeResult = null;
    double result;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView number1 = (TextView) findViewById(R.id.txt1);
        TextView number2 = (TextView) findViewById(R.id.txt2);
        TextView number3 = (TextView) findViewById(R.id.txt3);
        TextView number4 = (TextView) findViewById(R.id.txt4);
        TextView number5 = (TextView) findViewById(R.id.txt5);
        TextView number6 = (TextView) findViewById(R.id.txt6);
        TextView number7 = (TextView) findViewById(R.id.txt7);
        TextView number8 = (TextView) findViewById(R.id.txt8);
        TextView number9 = (TextView) findViewById(R.id.txt9);
        TextView number0 = (TextView) findViewById(R.id.txt0);
        TextView dot = (TextView) findViewById(R.id.txtdot);

        TextView delete = (TextView) findViewById(R.id.txtdel);
        TextView plus = (TextView) findViewById(R.id.txtplus);
        TextView minus = (TextView) findViewById(R.id.txtmin);
        TextView multi = (TextView) findViewById(R.id.txtmulti);
        TextView devision = (TextView) findViewById(R.id.txtdev);
        TextView equal = (TextView) findViewById(R.id.txtequal);
        final TextView Result = (TextView) findViewById(R.id.txtResult);

        //All Operator Button on Click Here
        OnClickListener operator = new OnClickListener() {

            @Override
            public void onClick(View view) {

                if (beforeResult == null) {
                    beforeResult = Result.getText().toString();
                    Result.setText("0");
                    return;
                } else {
                    afterResult = Result.getText().toString();

                }

                TextView operatorPress = (TextView) view;

                switch (operatorPress.getId()) {
                    case R.id.txtdel: {
                        Result.setText("0");
                    }
                    case R.id.txtplus: {

                        result = (Double.parseDouble(beforeResult) + Double.parseDouble(afterResult));

                    }
                    case R.id.txtmin: {

                    }
                    case R.id.txtmulti: {

                    }
                    case R.id.txtdev: {

                    }
                    case R.id.txtequal: {
                        Result.setText(" " + result);
                        beforeResult = null;
                    }

                }

            }

        };

        //All number Button on Click working here
        OnClickListener numbric = new OnClickListener() {

            @Override
            public void onClick(View view) {

                TextView numberPress = (TextView) view;
                String getTxtPressed = numberPress.getText().toString();

                if (getTxtPressed.equals(".")) {
                    if ( !Result.getText().toString().contains("."))
                        Result.setText(Result.getText().toString() + getTxtPressed);
                    return;
                }

                //ignore first zero
                if (Result.getText().toString().equals("0")) {
                    Result.setText(getTxtPressed);
                } else {
                    //only ten number pressed
                    if (Result.getText().length() < 11) {
                        Result.setText(Result.getText().toString() + getTxtPressed);
                    }
                }

            }
        };

        plus.setOnClickListener(operator);
        delete.setOnClickListener(operator);
        minus.setOnClickListener(operator);
        multi.setOnClickListener(operator);
        devision.setOnClickListener(operator);
        equal.setOnClickListener(operator);

        //--------------------------------------------------------------

        number0.setOnClickListener(numbric);
        number1.setOnClickListener(numbric);
        number2.setOnClickListener(numbric);
        number3.setOnClickListener(numbric);
        number4.setOnClickListener(numbric);
        number5.setOnClickListener(numbric);
        number6.setOnClickListener(numbric);
        number7.setOnClickListener(numbric);
        number8.setOnClickListener(numbric);
        number9.setOnClickListener(numbric);
        dot.setOnClickListener(numbric);

    }
}